
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>

#include "heap352.h"


void *root = NULL;

struct node *request_space(struct node* last, size_t size);

struct node
{
    int magic;
    int free352;
    size_t size;
    char *buffer;
    struct node *prev;
    struct node *next;
};

int NODE_SIZE = sizeof(struct node);

struct node *find_first(struct node **last, int size) 
{
  struct node *current = root;

  while (current && !(current->free352 && current->size >= size)) {
    *last = current;
    current = current->next;
  }
  return current;
}

void *malloc352(int nbytes) 
{
    struct node *block;

      if (nbytes <= 0) {
        return NULL;
      }

      if (!root) { // First call.
        block = request_space(root, nbytes);
        if (!block) {
          return NULL;
        }
        root = block;
      } else {
        struct node *last = root;
        block = find_first(&last, nbytes);
        if (!block) { // Failed to find free352 block.
          block = request_space(last, nbytes);
          if (!block) {
            return NULL;
          }
        } else {      // Found free352 block
          // TODO: consider splitting block here.
          block->free352 = 0;
          block->magic = 0x77777777;
        }
      }

      return(block+1);
        
}

struct node *request_space(struct node* last, size_t size) {
  struct node *block;

  block = sbrk(0);
  
  void *request = sbrk(size + NODE_SIZE);
  
  assert((void*)block == request); // Not thread safe.
  if (request == (void*) -1) {
    return NULL; // sbrk failed.
  }

  if (last) { // NULL on first request.
    last->next = block;
  }
  block->size = size;
  block->next = NULL;
  block->free352 = 0;
  block->magic = 0x12345678;
  return block;
}


struct node *get_block_ptr(void *ptr) {
  return (struct node*)ptr - 1;
}


void free352(void *ptr) {
  if (!ptr) {
    return;
  }

  // TODO: consider merging d once splitting blocks is implemented.
  struct node* block_ptr = get_block_ptr(ptr);
  assert(block_ptr->free352 == 0);
  assert(block_ptr->magic == 0x77777777 || block_ptr->magic == 0x12345678);
  block_ptr->free352 = 1;
  block_ptr->magic = 0x55555555;
}

/* Shell main */

int
main (void)
{
  for(int i = 0; i < 10; i++) {
    void *ptr = malloc(i);
    int *i = ptr;
    printf("Block %i at \n", *i);
  }
    
    free352(ptr);
}

